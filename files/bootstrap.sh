#!/bin/sh

echo "-----PYTHON SETUP"

python-pip install Flask redis



echo "-----INSTALL REDIS"


# Get redis source
cd /usr/local/src && wget http://download.redis.io/releases/redis-3.2.0.tar.gz
tar xzf redis-3.2.0.tar.gz && rm -f redis-3.2.0.tar.gz


# Compile redis
cd redis-3.2.0
make distclean
make
mkdir -p /etc/redis /var/lib/redis /var/redis/6379
cp src/redis-server src/redis-cli /usr/local/bin
cp redis.conf /etc/redis/6379.conf


# Prepare redis for use
sed -i 's/daemonize no/daemonize yes/g; s/dir .\//\/var\/redis\/6379/g' /etc/redis/6379.conf
wget https://raw.githubusercontent.com/saxenap/install-redis-amazon-linux-centos/master/redis-server
mv redis-server /etc/init.d
chmod 755 /etc/init.d/redis-server
sed -i 's/REDIS_CONF_FILE="\/etc\/redis\/redis.conf"/REDIS_CONF_FILE="\/etc\/redis\/6379.conf"/g; s/\/etc\/sysconfig\/networking/\/etc\/sysconfig\/network-scripts/g'  /etc/init.d/redis-server



echo "-----PHP-FPM SETUP"

sed -i 's/user = apache/user = nginx/g; s/group = apache/group = nginx/g' /etc/php-fpm.d/www.conf

echo "-----NGINX Setup"

mkdir /web
chown -R nginx:nginx /web
