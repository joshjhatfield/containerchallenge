import os
import sys 
from flask import Flask, request
from redishandler import CheckAndSubmit


App = Flask(__name__)

@App.route('/hello')
def hello():
    return "hello world"

@App.route('/submit', methods=['POST'])
def submitdata():
    FName = request.form['firstname']
    LName = request.form['lastname']
    Email = request.form['email']
    return CheckAndSubmit(FName, LName, Email)

@App.after_request
def add_headers(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    return response

if __name__ == '__main__':
  App.run(debug=True)