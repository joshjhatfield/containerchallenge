import redis

RedisServer = redis.StrictRedis(host='localhost', port=6379, db=0)

def CheckAndSubmit(UsrFName, UsrLName, UsrEmail):
    if RedisServer.get(UsrEmail + 'email') == UsrEmail:
    	CurUsrFName = RedisServer.get(UsrEmail + 'firstname')
    	CurUsrLName = RedisServer.get(UsrEmail + 'lastname')
        return ("This email address: %s is already being used by %s %s, try another." % (UsrEmail, CurUsrFName, CurUsrLName))
    else:
        RedisServer.set(UsrEmail + 'firstname', UsrFName)
        RedisServer.set(UsrEmail + 'lastname', UsrLName)
        RedisServer.set(UsrEmail + 'email', UsrEmail)
        return ("User %s %s has been registed with %s." % (UsrFName, UsrLName, UsrEmail))


