# Tech test #


### What does it do ? ###

The app is a mock registration form for a website which will accept a firstname, lastname and email which is stored after submitting, later users cannot register with the same email addess.

![registration](https://i.imgur.com/A7wikeo.png)

![already in use](https://i.imgur.com/NUjjE48.png)


However, my PHP knowledge is limited to the php post is not yet working correctly to the API, Testing the page I can see that the data is being posted
correctly yet not reaching the python API


### Technology Used ###

Docker - Docker is used to build this application and docker-compose is used to deploy the docker container, in a real work environment I would use multiple containers in an orchestrated environment.  

Python / Flask - Flask was used as the restful API framework for the http server, My main experience with writing api's has been with Python/Flask

Redis - Redis is used as the key value store for this web application, Redis was chosen as it is fast to implement and has a low setup overhead compared to SQL based databases, having experience in both I chose redis as in a real-world environment I would not run SQL inside a container. 

Nginx - In my work life I only use nginx for securing backend web applications and am more used to Apache and AWS Cloudfront however Nginx is designed to be a lighter web server, I selected Nginx as it seemed the most appropriate to use inside of a container, in the real world I would server on port 443 with a certificate.  

php-fpm - A better alternative to php-fastCGI

### To Run the app locally ###

To run this app you will need Docker and Docker compose

Both the container build and the deploy are built into a shell script to provide a full working demo, to run the script locally simply use

```
docker-compose up
```

To tear down the stack use

```
docker-compose down
```

You can now access this app via http://localhost/




### Who do I talk to? ###

Josh Hatfield - Author

